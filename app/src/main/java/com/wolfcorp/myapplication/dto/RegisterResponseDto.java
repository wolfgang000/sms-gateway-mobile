package com.wolfcorp.myapplication.dto;

import com.google.gson.annotations.SerializedName;

public class RegisterResponseDto {

    int id;

    @SerializedName("display_name")
    String displayName;

    @SerializedName("secret_key")
    String secretKey;

    RegisterResponseDto(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }
}
