package com.wolfcorp.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.wolfcorp.myapplication.dto.RegisterRequestDto;
import com.wolfcorp.myapplication.dto.RegisterResponseDto;

import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    RequestQueue queue;
    private Gson gson;
    public static final String SHARED_PREFERENCES_KEY_SECRET_KEY = "SECRET_KEY";
    public static final String SHARED_PREFERENCES_KEY_DEVICE_ID = "DEVICE_ID";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gson = new Gson();
        /*
        SharedPreferences sharedPref = this.getPreferences(Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.remove(SHARED_PREFERENCES_KEY_SECRET_KEY);
        editor.commit();
        */
        SharedPreferences sharedPref = this.getSharedPreferences("PREF1", Context.MODE_PRIVATE);
        String secretKey = sharedPref.getString(SHARED_PREFERENCES_KEY_SECRET_KEY, null);
        if(secretKey != null) {
            Log.i("TAG2","TOKEN: " + secretKey);
            Intent intent = new Intent(this, DashboardActivity.class);
            startActivity(intent);
        } else {
            registerDevice();
        }

    }

    private void registerDevice() {
        queue = Volley.newRequestQueue(this);
        String url = Config.API_URL + "/private_api/devices/register";


        JSONObject payload = new JSONObject();
        try {
            RegisterRequestDto registerRequest = new RegisterRequestDto();

            registerRequest.setLinkCode("abc123");
            registerRequest.setDisplayName("Moto G4");
            registerRequest.setRegistrationId("a1211A3ys");

            payload.put("device", new JSONObject(gson.toJson(registerRequest)));

        } catch (JSONException e) {
            e.printStackTrace();
        }

        final Activity activity = this;
        // Request a string response from the provided URL.
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, payload,
            new Response.Listener<JSONObject>() {
                    @Override
                    public void onResponse(JSONObject response) {
                        // Display the first 500 characters of the response string.
                        try {
                            RegisterResponseDto registerResponse = gson.fromJson(response.getJSONObject("data").toString(), RegisterResponseDto.class);
                            SharedPreferences sharedPref = activity.getSharedPreferences("PREF1", Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(SHARED_PREFERENCES_KEY_SECRET_KEY, registerResponse.getSecretKey());
                            editor.putInt(SHARED_PREFERENCES_KEY_DEVICE_ID, registerResponse.getId());
                            editor.commit();

                            Intent intent = new Intent(activity, DashboardActivity.class);
                            startActivity(intent);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        Log.i("TAG1","Response is: "+ response.toString());
                    }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.i("TAG1","That didn't work!");
                }
            });

        // Add the request to the RequestQueue.
        queue.add(request);
    }
}
