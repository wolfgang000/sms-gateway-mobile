package com.wolfcorp.myapplication;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;
import org.phoenixframework.channels.Channel;
import org.phoenixframework.channels.IErrorCallback;
import org.phoenixframework.channels.ISocketCloseCallback;
import org.phoenixframework.channels.ISocketOpenCallback;
import org.phoenixframework.channels.Socket;

import java.util.HashMap;
import java.util.Map;

public class DashboardActivity extends AppCompatActivity {

    private RequestQueue queue;
    private Gson gson;
    private Socket socket;
    private Channel channel;
    private String socketToken;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        queue = Volley.newRequestQueue(this);
        gson = new Gson();

        Log.i("TAG1", "START Dashboard");

        SharedPreferences sharedPref = this.getSharedPreferences("PREF1", Context.MODE_PRIVATE);
        String secretKey = sharedPref.getString(MainActivity.SHARED_PREFERENCES_KEY_SECRET_KEY, null);

        int deviceId = sharedPref.getInt(MainActivity.SHARED_PREFERENCES_KEY_DEVICE_ID, 0);
        final String deviceApiKey = Integer.toString(deviceId) + '.' + secretKey;
        Log.i("TAG1","ID: " + deviceId);

        Log.i("TAG1","deviceApiKey: " + deviceApiKey);

        if(secretKey != null) {
            Log.i("TAG1", "START REQUEST!");
            final Activity activity = this;

            JsonObjectRequest tokenRequest = new JsonObjectRequest(Request.Method.GET, Config.API_URL + "/private_api/devices/get_user_token", null,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.i("TAG1", " token: " + response);

                            try {
                                String token = response
                                        .getJSONObject("data")
                                        .getString("token");
                                ((DashboardActivity) activity).socketToken = token;
                                ((DashboardActivity) activity).connectToSocket();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.i("TAG1", "That didn't work!");
                        }
                    }) {
                @Override
                public Map<String, String> getHeaders() throws AuthFailureError {
                    Map<String, String> headers = new HashMap<>();
                    headers.put("authorization", "Token " + deviceApiKey);
                    return headers;
                }
            };
            queue.add(tokenRequest);

/*

            try {
                socket = new Socket(url.toString());


                socket.onOpen(new ISocketOpenCallback() {
                    @Override
                    public void onOpen() {
                        showToast("Connected");
                        channel = socket.chan(topic, null);

                        try {
                            channel.join().receive("ok", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    showToast("You have joined '" + topic + "'");
                                }
                            });
                            channel.on("user:entered", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    final JsonNode user = envelope.getPayload().get("user");
                                    if (user == null || user instanceof NullNode) {
                                        showToast("An anonymous user entered");
                                    }
                                    else {
                                        showToast("User '" + user.toString() + "' entered");
                                    }
                                }
                            }).on("new:msg", new IMessageCallback() {
                                @Override
                                public void onMessage(final Envelope envelope) {
                                    final ChatMessage message;
                                    try {
                                        message = objectMapper.treeToValue(envelope.getPayload(), ChatMessage.class);
                                        Log.i(TAG, "MESSAGE: " + message);
                                        if (message.getUserId() != null && !message.getUserId().equals("SYSTEM")) {
                                            addToList(message);
                                            notifyMessageReceived();
                                        }
                                    } catch (JsonProcessingException e) {
                                        Log.e(TAG, "Unable to parse message", e);
                                    }
                                }
                            });
                        } catch (Exception e) {
                            Log.e(TAG, "Failed to join channel " + topic, e);
                            handleTerminalError(e);
                        }
                        btnSend.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sendMessage();
                                messageField.setText("");
                            }
                        });
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                btnSend.setEnabled(true);
                            }
                        });
                    }
                })
                        .onClose(new ISocketCloseCallback() {
                            @Override
                            public void onClose() {
                                showToast("Closed");
                            }
                        })
                        .onError(new IErrorCallback() {
                            @Override
                            public void onError(final String reason) {
                                handleTerminalError(reason);
                            }
                        })
                        .connect();

            } catch (Exception e) {
                Log.e("TAG1", "Failed to connect", e);
                //handleTerminalError(e);
            }
*/



        }
    }
    void connectToSocket() {
        try {
            String url = "ws://10.0.2.2:4000/socket/websocket?token="+this.socketToken;
            socket = new Socket(url.toString());

            socket
                .onOpen(new ISocketOpenCallback() {
                    @Override
                    public void onOpen() {
                        Log.e("TAG1", "Connected");
                    }
                })
                .onClose(new ISocketCloseCallback() {
                        @Override
                        public void onClose() {
                            Log.e("TAG1", "Closed");
                        }
                    })
                    .onError(new IErrorCallback() {
                        @Override
                        public void onError(final String reason) {
                            Log.e("TAG1", "Error:" + reason);
                        }
                    })
                    .connect();

        } catch (Exception e) {
            Log.e("TAG1", "Failed to connect", e);
            //handleTerminalError(e);
        }
    }
}
